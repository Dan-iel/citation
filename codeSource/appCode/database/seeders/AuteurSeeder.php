<?php

namespace Database\Seeders;

use App\Models\Auteur;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\str;
class AuteurSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       /* DB::table('auteurs')->insert([
            "prenom" => Str::random(10),
            "nom" => Str::random(10)
        ]);
       This code works with thinker
       */

        Auteur::factory()
            ->count(10)
            ->create();
//php artisan db:seed --class=AuteurSeeder

    }
}
