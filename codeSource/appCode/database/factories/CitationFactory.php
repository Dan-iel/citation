<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Citation;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Citation>
 */
class CitationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
           'content' => $this->faker->paragraph(),
            'auteur_id' => 1,
        ];
    }

    public function run()
   {

       $citations = Citation::factory()
           ->count(3)
           ->create();

   }
}
