<?php

namespace Database\Factories;

use App\Models\Auteur;
use App\Models\Citation;
use Illuminate\Database\Eloquent\Factories\Factory;


/**
 * @extends Factory
 */
class AuteurFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    protected $model = Auteur::class;

    public function definition()
    {
        return [
            'prenom' => $this->faker->firstName(),
            'nom' => $this->faker->lastName()
        ];
    }

    public function run()
    {
        $auteur = Auteur::factory()
            ->count(10)
            ->create();
    }
    /* /**Auteur::factory()
         ->count(50)
         ->create();
    may be asked the commentes seesers code
      *
      * php artisan tinker
      * add all the commented code to plant seeds otherwise use database with php artisan migrate:fresh --seed
      * /*/


}
