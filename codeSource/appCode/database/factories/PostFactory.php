<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return PostFactory
     */
    public function definition()
    {
        return [
            'quote' = $this->faker->sentence($nbWords = 6, $variableNbWords = true);

        ];
    }

    protected static function newFactory()
    {
        return FlightFactory::new();
    }
}
