<x-layout>


    <x-slot name="title">
        @section('title', 'Home')
    </x-slot>


<div class="current">
    <p class="current-quote">" {{ $current->content }} "</p>
    <p class="current-auteur">~ {{ $current->auteur->nom.' '.$current->auteur->prenom }}</p>

</div>

</x-layout>
