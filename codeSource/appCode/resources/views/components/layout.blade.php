<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="{{'css/app.css'}}">
    <link rel="shortcut icon" href="{{ asset('images/logo.png') }}" size="16x16" type="image/png">
    <title>@yield('title') | citation</title>
</head>
<body>

    <nav class="top-nav">
        <div class="logo">
            <img src="{{asset('images/logo.png')}}" alt="logo" srcset="">
            <p class="name">Citataion.fr</p>
        </div>

        <input id="menu-toggle" type="checkbox"/>
        <label class='menu-button-container' for="menu-toggle">
            <div class='menu-button'></div>
        </label>

        <ul id="menu">
            <li><a href="/">Home page</a></li>
            <li><a href="citations">Les citations</a></li>
            <li><a href="auteurs">Les auteurs</a></li>
            <li><a href="search">Les citations d'un auteur</a></li>
        </ul>
    </nav>

    <main class="flex-grow">
        {{$slot}}
    </main>

    <footer aria-labelledby="footer-heading fixed inset-x-0 bottom-0">
        <h2 id="footer-heading" class="sr-only">&copy; Copyright 2022 citation.fr</h2>

    </footer>
    <script src="{{'js/app.js'}}"></script>

</body>
</html>


<div>
    <!-- Walk as if you are kissing the Earth with your feet. - Thich Nhat Hanh -->
</div>
