<x-layout>


    <x-slot name="title">
        @section('title', 'Auteurs')
    </x-slot>


    @foreach ($auteurs as $auteur)
        <p>This is Auteur_id {{ $auteur->id }}</p>
        <p>This is Auteur_n&p {{ $auteur->nom.' '.$auteur->prenom }}</p>
    @endforeach

    {{ $auteurs->links() }}
</x-layout>
