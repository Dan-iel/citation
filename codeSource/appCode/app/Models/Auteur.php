<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Auteur extends Model
{
    use HasFactory;
    protected $fillable = [
        'prenom',
        'nom',
    ];

    /*public function run()
    {
        User::factory()
            ->count(50)
            ->create();
    this code works with php artisan thinker (execute as a line of code)
    may be demander
    }*/

    public function auteur()
    {
        return $this->belongsTo(Auteur::class, 'auteur_id');
    }

}
