<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Auteur;

class Citation extends Model
{
    use HasFactory;
    protected $fillable = [
        'content',
        'auteur_id'
    ];

    /**
     * Get the post that owns the comment.
     */
    public function auteur()
    {
        return $this->belongsTo(Auteur::class, 'auteur_id');
    }
}
