<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Citation;

class CitationController extends Controller
{
    //
    public function index() {
        $citations = Citation::paginate(10);
        return view('citation', [
            'citations' => $citations
        ]);
    }

    public function toxscreen(){
        $current = Citation::orderBy('id', 'DESC')->first();
        return view('home', [
            'current'=>$current
        ]);
    }

    public function search() {
        $quotes = Citation::select('content')->where('auteur_id', 1)->paginate(10);
        return view('search', [
            'quotes' => $quotes
        ]);
    }

}
