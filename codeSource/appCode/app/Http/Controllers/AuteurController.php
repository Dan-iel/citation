<?php

namespace App\Http\Controllers;

use App\Models\Citation;
use Illuminate\Http\Request;
use App\Models\Auteur;
class AuteurController extends Controller
{
    public function index() {
        $auteurs = Auteur::paginate(10);
        return view('auteurs', [
            'auteurs' => $auteurs
        ]);
    }
}
